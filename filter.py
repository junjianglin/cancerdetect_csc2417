import pandas as pd
import numpy as np
from sklearn import feature_selection
from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression
from infogain import MixtureQuantizer, information_gain
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier

iscancer = lambda a : 1 if a.endswith("_CRC") else 0

def load_data(datafile):
    X = pd.DataFrame.from_csv(datafile, sep='\t', header=0)
    X = X.transpose()
    y = np.array([iscancer(_) for _ in X.index], dtype=np.int8)
    return X.as_matrix(), y, X.index, X.columns

filters = {
        'variancethreshold' : [
            {'class' : feature_selection.VarianceThreshold,
             'args' : [('threshold', float, 0.5)]}],
        'chi2' : [
            {'class' : feature_selection.SelectKBest,
             'args' : [('k', int, 40)],
             'fixed' : {'score_func' : feature_selection.chi2}}],
        'linearsvc' : [
            {'class' : LinearSVC,
             'args' : [('penalty', str, 'l1'),
                       ('dual', bool, False),
                       ('loss', str, 'squared_hinge')]}],
        'logisticregression' : [
            {'class' : LogisticRegression,
             'args'  : [('penalty', str, 'l1'),
                        ('dual', bool, False)]}],
        'infogain' : [
            {'class' : MixtureQuantizer,
             'args' : []},
            {'class' : feature_selection.SelectKBest,
             'args' : [('k', int, 40)],
             'fixed' : {'score_func' : information_gain}}]}


def _output_features(feature_indices, feature_labels, scores, output):
        with open(output, 'w') as f:
            for i in feature_indices:
                if scores is not None:
                    f.write("%s %f\n" % (feature_labels[i], scores[i]))
                else:
                    f.write("%s\n" % feature_labels[i])

def output_features(selector, feature_labels, scores, output):
    if hasattr(selector, 'get_support'):
            feature_indices = selector.get_support(indices=True)
            _output_features(feature_indices, feature_labels, scores, output)
    else:
        # Now we need to do some work to extract the features
        if isinstance(selector, LinearSVC) or isinstance(selector, LogisticRegression):
            importances = np.sum(np.abs(selector.coef_), axis=0)
            mask = importances >= 1e-5
            feature_indices = [i for i, b in enumerate(mask) if b]
            _output_features(feature_indices, feature_labels, scores, output)
        else:
            raise NotImplementedError()
    print("selected features output to %s" % output)


def add_arguments(parser):
    subparsers = parser.add_subparsers(dest='name')
    for name in filters:
        subparser = subparsers.add_parser(name)
        for cls in filters[name]:
            for arg, arg_type, arg_default in cls['args']:
                subparser.add_argument("--%s" % arg, type=arg_type, default=arg_default)


def get_arguments(args):
    filter_kwargs = []
    for cls in filters[args.name]:
        kwargs = {}
        for arg, _,_ in cls['args']:
            kwargs[arg] = args.__getattribute__(arg)
        if 'fixed' in cls:
            kwargs.update(cls['fixed'])
        filter_kwargs.append(kwargs)
    return filter_kwargs


if __name__ == "__main__":
    import sys, argparse, time, logging
    logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)
    parser = argparse.ArgumentParser(description="Select features using\
            variance threshold method")
    parser.add_argument('--train', required=True, type=str, help="(Required) \
            The training dataset.")
    parser.add_argument('--test', type=str, help="(Optional) The testing \
            dataset. If specified then the model will run on the testing data \
            and testing score will be reported.")
    parser.add_argument('--output', type=str, help="(Optional) If sepcified, \
            the selected features will be output to a file with each feature \
            name separated by a new line.")
    add_arguments(parser)
    args = parser.parse_args(sys.argv[1:])
    filter_kwargs = get_arguments(args)
    
    # Load training data
    start = time.clock()
    X, y, sample_labels, feature_labels = load_data(args.train)
    duration = time.clock() - start
    logging.info("Loaded training data file %s in %.4f sec" % (args.train, duration))

    # Create feature selector evaluation pipeline
    steps = []
    for i, (step_class, kwargs) in enumerate(zip(filters[args.name], filter_kwargs)):
        steps.append(('feature_selection_%d' % i, step_class['class'](**kwargs)))
    selector = steps[-1][1]
    steps.append(('classification', RandomForestClassifier()))
    pipeline = Pipeline(steps)
    classifer = pipeline.fit(X, y)
    logging.info("Classifer score (training): %.4f" % classifer.score(X, y))

    # Output selected features if specified
    if args.output is not None:
        if args.name in ['infogain', 'chi2']:
            assert(hasattr(selector, 'scores_'))
            output_features(selector, feature_labels, selector.scores_, args.output)
        else:
            output_features(selector, feature_labels, None, args.output)

    # Run testing if a testing dataset is specified
    if args.test is not None:
        # Load testing data
        start = time.clock()
        X_test, y_test, _, _ = load_data(args.test)
        duration = time.clock() - start
        logging.info("Loaded testing data file %s in %.4f sec" % (args.test, duration))

        # Compute the score on testing data
        logging.info("Classifer score (testing): %.4f" % classifer.score(X_test, y_test))
