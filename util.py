import pandas as pd
import numpy as np

iscancer = lambda a : 1 if a.endswith("_CRC") else 0


def load_dataframe(datafile):
    d = pd.DataFrame.from_csv(datafile, sep='\t', header=0)
    d = d.transpose()
    y = np.array([iscancer(_) for _ in d.index], dtype=np.int8)
    return d, y

def load_data(datafile):
    d = pd.DataFrame.from_csv(datafile, sep='\t', header=0)
    d = d.transpose()
    y = np.array([iscancer(_) for _ in d.index], dtype=np.int8)
    return d.as_matrix(), y, d.index, d.columns


def load_selected_data(datafile, selected_features):
    d, y = load_dataframe(datafile)
    d = d[selected_features]
    return d.as_matrix(), y, d.index, d.columns
