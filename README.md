Here is a repo for cancer feature detection project in csc2417

Contributors: Eric Zhu, Junjiang lin


## Information Gain Filter

First quantize the feature values to 0s and 1s using Gaussian Mixture Model
(GMM).
Fit a 1-D, 2 components GMM to each feature separately.
Assign each feature value 0 or 1 based on its posterior probability given
the GMM of this feature.

Then compute the information gain of each feature.

Use `infogain.py` to run the above steps. Run `python infogain.py -h` to
see helps.

Once you have the output from `infogain.py`, 
select the top 500 features based on their information gain, run:

```
python select_kbest.py --k 500 --precision 3 --sort-by infogain <features> <features_top500>
```

`<features>` is the output from `infogain.py`, `<features_top500>` is the
output which contains the selected features.

The `--precision` parameter specifies the number of decimal digits to consider
when ranking the features based on their information gains or mixture overlap
probabilities. Run `python select_kbest.py -h` for more helps.
