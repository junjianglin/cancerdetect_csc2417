#-------------------INFO----------------------
#
# author: Junjiang Lin
# date: 2015-03-20
# email: junjianglin@cs.toronto.com

#
# This r script is used to streamline the building of classifier
#
# Input List:
# 1. Training data( RData or txt)
# 2. Training labels(txt)
# 3. Testing data(RData or txt)
# 4. Testing labels(txt)
#
# Output List:
# 1. Primary output name


# This r script is used to do classification with 
# 1.svm, 
# 2.random forest, 
# 3.partial least squres,
# 4.recursive partitioning, 
# 5.stochastic gradient boosting, 
# 6.c5.0,
# 7.boosted logistic regression


#######################   Initialization   ################
library(optparse)
rm(list=ls(all=TRUE))


option_list = list(
        make_option(c("-i","--training"),dest = "training",
                    help="the training set RData"),
        make_option(c("-t","--testing"),dest = "testing",
                    help="the testing set RData"),
        make_option(c("-I","--trainingLabel"),dest = "trainingLabel",
                    help="label file for training data"),
        make_option(c("-T","--testingLabel"),dest = "testingLabel",
                    help="label file for testing data"),
        make_option(c("-o","--output"),dest = "output",
                    help="the primary name of output file"),
        make_option(c("-s","--seed"),dest = "seed",default=1,type="integer",
                    help="the seed, [Default] = 1")
        )
opt = parse_args(OptionParser(option_list=option_list))
if(is.null(opt$training) || is.null(opt$output)|| is.null(opt$testing)|| is.null(opt$testingLabel)|| is.null(opt$trainingLabel)){
        print_help(OptionParser(option_list=option_list))
        q(save = "no")
}
              
#libraries to load
library(caret)
library(ROCR)
library(scales)
library(tools)
library(ggplot2)


cat("\n","Initializing the environment and loading data...","\n")
seed = opt$seed
set.seed(seed)
training = opt$training
testing = opt$testing
trainingLabel = opt$trainingLabel
testingLabel = opt$testingLabel
output = opt$output
# load or read training data
if(file_ext(training) == 'txt'){
    trainSet = read.delim(file = training)
} else if(file_ext(training) == 'RData'){
    trainSet = get(load(training))
}

# load or read testing data
if(file_ext(testing) == 'txt'){
    testSet = read.delim(file = testing)
} else if(file_ext(testing) == 'RData'){
    testSet = get(load(testing))
}

trainLabel = as.factor(read.table(file=trainingLabel)[,1])
testLabel = as.factor(read.table(file=testingLabel)[,1])


#######################   Outscourcing Functions  #############
# ROC
roceval <- function(myscores, labels_true, model) {
        
        pred <- prediction(myscores, labels_true)
        
        perf <- performance( pred, "tpr", "fpr" )
        
        auc <- unlist(slot(performance( pred, "auc" ), "y.values"))
        
        fpr <- unlist(slot(perf, "x.values"))
        
        tpr <- unlist(slot(perf, "y.values"))
        
        cutoffval <- unlist(slot(perf, "alpha.values"))	
        
        rocdf <- data.frame(x= fpr, y=tpr, auc=auc, 
                            
                            cutoff=cutoffval, 
                            
                            evalTypeAUC=sprintf("%s (%s)", model, percent(auc)), 
                            
                            model=model, curveType="ROC")
        
        return(rocdf)
}


# PRC
prceval <- function(myscores, labels_true, model) {
        
        pred <- prediction(myscores, labels_true)
        
        perf <- performance( pred, "prec", "rec" )
        
        rec <- unlist(slot(perf, "x.values"))
        
        prec <- unlist(slot(perf, "y.values"))
        
        cutoffval <- unlist(slot(perf, "alpha.values"))	
        
        prec[is.nan(prec)] <- 0
        
        prec[length(prec)] <- 0
        
        rec[is.nan(rec)] <- 0
        
        auc <- integrate(approxfun(cbind(rec, prec)), lower=0, upper=1,
                         subdivisions=1e5, stop.on.error = FALSE)$value					
        
        prcdf <- data.frame(x=rec, y=prec, auc=auc,
                            evalTypeAUC=sprintf("%s (%s)", model, 
                                                percent(auc)), model=model, curveType="PRC")
        
        return(prcdf)
}

#plotting
evalLinePlot <- function(mydf, curve, mytitle=NA) {
        
        if(curve=="ROC") {
                x.title <- "False positive rate"	
                y.title <- "True positive rate"
        } else {
                x.title <- "Recall"	
                y.title <- "Precision"		
        }
        
        gg <- ggplot(mydf, aes(x=x, y=y, color=evalTypeAUC)) + 
                
                theme_bw() +
                
                geom_line(aes(linetype=evalTypeAUC), size=0.7) +
                
                scale_x_continuous(x.title, labels=percent) +
                
                scale_y_continuous(y.title, labels=percent) + 		
                
                theme(axis.text.x = element_text(colour = "black"), 
                      
                      axis.text.y = element_text(colour = "black"),
                      
                      axis.title.x = element_text(colour = "black"),				
                      
                      legend.title= element_blank(),	
                      
                      legend.position=c(0.8,0.2),
                      
                      plot.title = element_text(colour = "black"),
                      
                      legend.background = element_blank())
        
        if(!is.na(mytitle)) gg <- gg + ggtitle(mytitle)
        
        if(curve=="ROC") gg + 
                geom_abline(intercept = 0, slope = 1, colour="grey", linetype=2) else gg	
}

#######################   Preprocessing  ################
# Changing all features' name to valid R names
cat("\n","preprocessing the data","\n")
#if(ncol(trainSet) == 1){
#        cat("\n","No training features","\n")
#        out = data.frame(name = opt$input,numOfFeatures=0)
#        write.table(out,file=opt$summary,quote=F,append=T,col.names=F,row.names=F)
#        q(save="no")
#}

names(trainSet) = make.names(names(trainSet))
names(testSet) = make.names(names(testSet))

#control should be in a higher level
case = "CRC"
control = "CTR"
trainLabel = factor(trainLabel,levels=c(control,case))
testLabel = factor(testLabel,levels=c(control,case))

#######################   1.SVM(RBF) with repeatedcv  ###############
cat("\n","SVM with repeatedcv","\n")
svmctrl = trainControl(method = 'repeatedcv',
                       repeats=1,
                       summaryFunction = twoClassSummary,
                       classProbs = TRUE)
#tuning parameters,  C from 0.1 to 100, and log C is equally spaced
# sigma from 0.1 to 10, and log sigma is equally spaced
svmgrid = expand.grid(C = exp(seq(log(0.01),log(10),length=15)),
                      sigma = exp(seq(log(0.01),log(10),length=15)))

svm_tr = train(trainLabel ~ .,data=trainSet,
              method = 'svmRadial',
              trControl = svmctrl,
              tuneGrid = svmgrid,
              metric = 'ROC',
              preProc = c("center","scale"))

svm_pred = predict(svm_tr,newdata=testSet)
svm_pred_prob = predict(svm_tr,newdata = testSet, type='prob')
svm_table = confusionMatrix(data = svm_pred, testLabel)
svm_roc = roceval(svm_pred_prob[[case]], as.numeric(testLabel), "SVM")
svm_gg =  evalLinePlot(svm_roc, "ROC", "Test ROC for SVM")  
#ggsave(sprintf("%s/svm_9.png", ggout), gg_svm, width=6.4, height=4.8)

######################    2.Partial Least Squares with repeatedcv  ####################
#cat("\n","Partial least squares with repeatedcv","\n")
#plsctrl = trainControl(method = 'repeatedcv',
#                       repeats = 3,
#                       classProbs = T,
#                       summaryFunction = twoClassSummary)
#pls_tr = train(type ~ ., data = trainSet,
#              method = 'pls',
#              tuneLength = 15,
#              trControl = plsctrl,
#              metric = 'ROC',
#              preProc = c("center","scale"))

#pls_pred = predict(pls_tr,newdata=testSet)
#pls_pred_prob = predict(pls_tr,newdata=testSet, type = 'prob')
#pred_tr_pls = predict(pls_tr,newdata=trainSet)
#pls_table = confusionMatrix(data = pls_pred, testSet$type)
#pls_roc = roceval(pls_pred_prob[[case]], as.numeric(testSet$type), "PLS")
#pls_gg =  evalLinePlot(pls_roc, "ROC", "Test ROC for Partial Least Squares")  
#ggsave(sprintf("%s/pls_9.png", ggout), gg_pls, width=6.4, height=4.8)

#######################   3.General Random Forest with repeated cv  #################
cat("\n","general random forest with repeatedcv","\n")
rfctrl = trainControl(method = 'repeatedcv',
                      repeats = 3,
                      classProbs = T,
                      summaryFunction = twoClassSummary)

rf_tr = train(trainLabel ~ ., data = trainSet,
             method = 'rf',
             tuneLength = 15,
             trControl = rfctrl,
             metric = 'ROC',
             preProc = c("center","scale"))

rf_pred = predict(rf_tr,newdata=testSet)
rf_pred_prob = predict(rf_tr,newdata=testSet,type = "prob")
rf_table = confusionMatrix(data = rf_pred, testLabel)
rf_roc = roceval(rf_pred_prob[[case]], as.numeric(testLabel), "RF")
rf_gg =  evalLinePlot(rf_roc, "ROC", "Test ROC for Random Forest")  
#ggsave(sprintf("%s/rf_9.png", ggout), gg_rf, width=6.4, height=4.8)

######################   4.Recursive Partitioning with repeatedcv ###########
cat("\n","recursive partitioning with repeatedcv","\n")
rpctrl = trainControl(method = 'repeatedcv',
                      repeats = 3,
                      classProbs = T,
                      summaryFunction = twoClassSummary)

rp_tr = train(trainLabel ~ ., data = trainSet,
             method = 'rpart',
             tuneLength = 15,
             trControl = rpctrl,
             metric = 'ROC',
             preProc = c("center","scale"))

rp_pred = predict(rp_tr,newdata=testSet)
rp_pred_prob = predict(rp_tr,newdata=testSet,type = "prob")
rp_table = confusionMatrix(data = rp_pred, testLabel)
rp_roc = roceval(rp_pred_prob[[case]], as.numeric(testLabel), "RP")
rp_gg =  evalLinePlot(rf_roc, "ROC", "Test ROC for Recursive Partitioning")  
#ggsave(sprintf("%s/rp_9.png", ggout), gg_rf, width=6.4, height=4.8)

##################### 5.Stochastic Gradient Boosting with repeatedcv #################
cat("\n","stochastic gradient boosting with repeatedcv","\n")
gbmctrl = trainControl(method = 'repeatedcv',
                      repeats = 2,
                      classProbs = T,
                      summaryFunction = twoClassSummary)

gbmGrid = expand.grid(interaction.depth = c(1, 5, 9),
                        n.trees = (1:30)*50,
                        shrinkage = 0.1)
gbm_tr = train(trainLabel ~ ., data = trainSet,
              method = 'gbm',
              tuneGrid = gbmGrid,
              verbose = FALSE,
              trControl = gbmctrl,
              metric = 'ROC',
              preProc = c("center","scale"))

gbm_pred = predict(gbm_tr,newdata=testSet)
gbm_pred_prob = predict(gbm_tr,newdata=testSet,type = "prob")
gbm_table = confusionMatrix(data = gbm_pred, testLabel)
gbm_roc = roceval(gbm_pred_prob[[case]], as.numeric(testLabel), "gbm")
gbm_gg =  evalLinePlot(gbm_roc, "ROC", "Test ROC for Stochastic Gradient Boosting")  

##################### 6. C5.0 with repeatedcv #################
cat("\n","C5.0 with repeatedcv","\n")
c50ctrl = trainControl(method = 'repeatedcv',
                       repeats = 3,
                       classProbs = T,
                       summaryFunction = twoClassSummary)
c50Grid = expand.grid(model="tree",
                      trials=c(1:100),
                      winnow=FALSE)
c50_tr = train(trainLabel ~ ., data = trainSet,
               method = 'C5.0',
               tuneGrid = c50Grid,
               trControl = c50ctrl,
               metric = 'ROC',
               preProc = c("center","scale"))

c50_pred = predict(c50_tr,newdata=testSet)
c50_pred_prob = predict(c50_tr,newdata=testSet,type = "prob")
c50_table = confusionMatrix(data = c50_pred, testLabel)
c50_roc = roceval(c50_pred_prob[[case]], as.numeric(testLabel), "C5.0")
c50_gg =  evalLinePlot(c50_roc, "ROC", "Test ROC for C5.0")  

##################### 7.boosted logistic regression with repeatedcv #################
cat("\n","boosted logistic regression with repeatedcv","\n")
logitctrl = trainControl(method = 'repeatedcv',
                       repeats = 3,
                       classProbs = T,
                       summaryFunction = twoClassSummary)

logit_tr = train(trainLabel ~ ., data = trainSet,
               method = 'LogitBoost',
               tuneLength = 10,
               trControl = logitctrl,
               metric = 'ROC',
               preProc = c("center","scale"))

logit_pred = predict(logit_tr,newdata=testSet)
logit_pred_prob = predict(logit_tr,newdata=testSet,type = "prob")
logit_table = confusionMatrix(data = logit_pred, testLabel)
logit_roc = roceval(logit_pred_prob[[case]], as.numeric(testLabel), "logitBoost")
logit_gg =  evalLinePlot(logit_roc, "ROC", "Test ROC for boosted logistic regression")  


#####################  Simple Voting  ################
predDF = data.frame(svm_pred,
#                    pls_pred,
                    rf_pred,
                    rp_pred,
                    gbm_pred,
                    c50_pred,
                    logit_pred)

comb_pred = factor(apply(predDF,1,function(row) ifelse(mean(row==case) > 0.5,case,control)),
                   levels=(c(control,case)))
comb_table = confusionMatrix(data = comb_pred, testLabel)
predDF$comb_pred = comb_pred

######################   Summary   ##################
cat("\n","generating summary","\n")
# ROC Curve 
#combineRoc = rbind(rf_roc,svm_roc,pls_roc,rp_roc,gbm_roc,c50_roc,logit_roc)
combineRoc = rbind(rf_roc,
		   svm_roc,
#		   pls_roc,
		   rp_roc,
		   gbm_roc,
		   c50_roc,
		   logit_roc)
all_gg = evalLinePlot(combineRoc, "ROC", "Test ROC for All Models")  


auc_rf = max(as.numeric(do.call(rbind,strsplit(as.character(rf_roc$evalTypeAUC),'[(%)]'))[,2]))
auc_svm = max(as.numeric(do.call(rbind,strsplit(as.character(svm_roc$evalTypeAUC),'[(%)]'))[,2]))
#auc_pls = max(as.numeric(do.call(rbind,strsplit(as.character(pls_roc$evalTypeAUC),'[(%)]'))[,2]))
auc_rp = max(as.numeric(do.call(rbind,strsplit(as.character(rp_roc$evalTypeAUC),'[(%)]'))[,2]))
auc_gbm = max(as.numeric(do.call(rbind,strsplit(as.character(gbm_roc$evalTypeAUC),'[(%)]'))[,2]))
auc_c50 = max(as.numeric(do.call(rbind,strsplit(as.character(c50_roc$evalTypeAUC),'[(%)]'))[,2]))
auc_logit = max(as.numeric(do.call(rbind,strsplit(as.character(logit_roc$evalTypeAUC),'[(%)]'))[,2]))
auc_best = max(as.numeric(do.call(rbind,strsplit(as.character(combineRoc$evalTypeAUC),'[(%)]'))[,2]))
out = data.frame(name = output,
                 numOfFeatures=ncol(trainSet),
                 auc_svm = auc_svm,
                 auc_rf=auc_rf,
#                 auc_pls = auc_pls,
                 auc_rp = auc_rp,
                 auc_gbm = auc_gbm,
                 auc_c50 = auc_c50,
                 auc_logit = auc_logit,
                 auc_best = auc_best)
write.table(out,file=paste(output,"_MLSummary",".txt",sep=""),quote=F,col.names=T,row.names=F,sep="\t")
ggsave(all_gg,file=paste(output,"_MLROC",".pdf",sep=""))
