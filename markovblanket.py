'''
Implements markove blanket feature selection
algorihtm.
'''
import logging
import pandas as pd
import numpy as np

from util import load_selected_data
from infogain import quantize

def pairwise_corr_ranked(X):
    '''
    Compute pairwise correlation between features and
    return a ranked list of most correlated features for each features.
    '''
    _, n_features = X.shape
    corr = np.corrcoef(X, rowvar=0)
    corr = np.absolute(corr)
    ranks = np.zeros((n_features, n_features-1), dtype=np.int)
    for i in range(n_features):
        ranks[i,:] = [j for j in np.argsort(-corr[i,:]) if j != i]
    return ranks

def count_M(X, y, M, f):
    '''
    Count the freequencies required to compute MLE of the model probabilities
    of markov blanket M for feature f
    f is a column index of data X, representing the feature
    M is a list of indices of the columns in data X, M is the feature subset
    representing the markov blanket for feature f
    '''
    counts = {}
    n, m = X.shape
    for i in range(n):
        t = tuple(X[i,M])
        if t not in counts:
            counts[t] = {'total' : 0, 'f=0' : {'total' : 0, 'y=0' : 0, 'y=1' : 0}, 
                                      'f=1' : {'total' : 0, 'y=0' : 0, 'y=1' : 0}}
        counts[t]['total'] += 1
        if X[i,f] == 1:
            counts[t]['f=1']['total'] += 1
            if y[i] == 1:
                counts[t]['f=1']['y=1'] += 1
            if y[i] == 0:
                counts[t]['f=1']['y=0'] += 1
        elif X[i,f] == 0:
            counts[t]['f=0']['total'] += 1
            if y[i] == 1:
                counts[t]['f=0']['y=1'] += 1
            if y[i] == 0:
                counts[t]['f=0']['y=0'] += 1
        else:
            raise ValueError("Data is not quantized")
    return counts

def calc_delta(counts, n_samples):
    delta = 0
    for f_M in counts:
        count = counts[f_M]
        d = 0
        # Joint probs of P(M=f_M,F_i=f_i)
        P_fm_fi_0 = float(count['f=0']['total'])/float(n_samples)
        P_fm_fi_1 = float(count['f=1']['total'])/float(n_samples)
        # Conditional probs P(y=0|M=f_M) and P(y=1|M=f_M)
        P_fm_y_0 = float(count['f=0']['y=0']+count['f=1']['y=0'])/float(count['total']) 
        P_fm_y_1 = float(count['f=0']['y=1']+count['f=1']['y=1'])/float(count['total'])
        #logging.info("P_fm_fi_0 = %f, P_fm_fi_1 = %f" % (P_fm_fi_0, P_fm_fi_1))
        #logging.info("P_fm_y_0 = %f, P_fm_y_1 = %f" % (P_fm_y_0, P_fm_y_1))
        if P_fm_fi_0 != 0:
            # Conditional probs P(y=0|M=f_M,F_i=0)
            P_fm_fi_0_y_0 = float(count['f=0']['y=0'])/float(count['f=0']['total'])
            # Conditional probs P(y=1|M=f_M,F_i=0)
            P_fm_fi_0_y_1 = float(count['f=0']['y=1'])/float(count['f=0']['total'])
            # Compute the KL-divergence
            kl = 0
            if P_fm_fi_0_y_0 != 0:
                kl += P_fm_fi_0_y_0 * np.log2(P_fm_fi_0_y_0 / P_fm_y_0)
            if P_fm_fi_0_y_1 != 0:
                kl += P_fm_fi_0_y_1 * np.log2(P_fm_fi_0_y_1 / P_fm_y_1)
            kl = np.exp2(np.log2(P_fm_fi_0)+np.log2(kl))
            d += kl
        if P_fm_fi_1 != 0:
            # Conditional probs P(y=0|M=f_M,F_i=1)
            P_fm_fi_1_y_0 = float(count['f=1']['y=0'])/float(count['f=1']['total'])
            # Conditional probs P(y=1|M=f_M,F_i=1)
            P_fm_fi_1_y_1 = float(count['f=1']['y=1'])/float(count['f=1']['total'])
            # Compute the KL-divergence
            kl = 0
            if P_fm_fi_1_y_0 != 0:
                kl += P_fm_fi_1_y_0 * np.log2(P_fm_fi_1_y_0 / P_fm_y_0)
            if P_fm_fi_1_y_1 != 0:
                kl += P_fm_fi_1_y_1 * np.log2(P_fm_fi_1_y_1 / P_fm_y_1)
            kl = np.exp2(np.log2(P_fm_fi_1) + np.log2(kl))
            d += kl
        if d < 0.0:
            raise ValueError("KL Divergence cannot be negative or undefined")
        delta += d
    return delta

def markove_blanket(X, y, corr_ranks, feature_labels, k=None):
    n_samples, n_features = X.shape 
    removed_fi = np.zeros((n_features,), dtype=np.int)
    delta_fi = np.zeros((n_features,), dtype=np.float)
    iter_count = 0
    G = set(range(n_features))
    # Iterate until there is only one feature left
    while len(G) > 1:
        f_to_remove = None
        min_delta = np.inf
        for f in G:
            # Find the features with higest correlations 
            # to create a markov blanket
            if k is None:
                M = [j for j in corr_ranks[f,:] if j in G]
            else:
                M = [j for j in corr_ranks[f,:] if j in G][:k]
            # Compute the delta using conditional entroy
            counts = count_M(X, y, M, f)
            delta = calc_delta(counts, n_samples)
            if delta < min_delta:
                f_to_remove = f
                min_delta = delta
        # Update G by remove the most irrelevant feature
        G.remove(f_to_remove)
        # Update result
        removed_fi[iter_count] = f_to_remove
        delta_fi[iter_count] = min_delta
        logging.info("Iteration %d: removed feature %s with delta %.4f" 
                % (iter_count, feature_labels[f_to_remove], min_delta))
        iter_count += 1
    # Add the last feature, set its delta to nan
    removed_fi[iter_count] = G.pop()
    delta_fi[iter_count] = np.nan
    return removed_fi, delta_fi 

if __name__ == "__main__":
    import argparse, sys
    logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument('--k', type=int, default=-1,
            help="The number of features to use when constructing a Markov Blanket\
                    for some feature, must be less or equal to n.\
                    If not sepecified, all other features will be considered.")
    parser.add_argument('data', type=str, help="The traning data")
    parser.add_argument('input', type=str, help="The input file of \
            selected features from select_kbest")
    parser.add_argument('output', type=str, help="The output file for \
            the sequence of feature removal by Markov Blanket Filtering algorithm")
    args = parser.parse_args(sys.argv[1:])
    if args.k < 0:
        args.k = None

    logging.info("Loading input features %s" % args.input)
    input_features = pd.DataFrame.from_csv(args.input)

    logging.info("Loading data file %s and selecting %d features" 
            % (args.data, len(input_features.index)))
    X, y, sample_labels, feature_labels = load_selected_data(args.data,
            input_features.index)

    logging.info("Computing pairwise correlations of features")
    corr = pairwise_corr_ranked(X)

    logging.info("Quantizing data using 1-D GMM with 2 components")
    mixture_overlap = quantize(X, y)

    logging.info("Start Markov Blanket Filtering algorithm")
    remove_seq, deltas = markove_blanket(X, y, corr, feature_labels, args.k)
    remove_seq_features = feature_labels[remove_seq]

    logging.info("Output the sequence of feature removals to %s"
            % args.output)
    df = pd.DataFrame(deltas, index=remove_seq_features, columns=['delta'])
    df.to_csv(args.output)
