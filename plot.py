import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def plot_mixture_overlap_prob(args):
    features = pd.DataFrame.from_csv(args.features).sort('mixture_overlap_prob')
    e = features['mixture_overlap_prob']
    plt.plot(range(len(e)), e)
    plt.title("Predictive power of genes")
    plt.xlabel("Gene")
    plt.ylabel("Bayes error")
    plt.savefig(args.output)
    plt.close()

def plot_information_gain(args):
    features = pd.DataFrame.from_csv(args.features).sort('infogain')
    e = features['infogain']
    plt.plot(range(len(e)), e)
    plt.title("Information gain for each gene with respect to the cancer label")
    plt.xlabel("Gene")
    plt.ylabel("Information gain")
    plt.savefig(args.output)
    plt.close()

def plot_markovblanket(args):
    features = pd.DataFrame.from_csv(args.features)
    e = features['delta']
    plt.plot(range(len(e)), e)
    plt.title("KL Divergence of each removal gene w.r.t. its Markov Blanket")
    plt.xlabel("Gene")
    plt.ylabel("KL Divergence")
    plt.savefig(args.output)
    plt.close()

plot_funcs = {'mixture_overlap_prob' : plot_mixture_overlap_prob, 
              'info_gain' : plot_information_gain,
              'markov_blanket' : plot_markovblanket}

if __name__ == "__main__":
    import argparse, sys
    parser = argparse.ArgumentParser('Plot the result from information gain filter.')
    parser.add_argument('--plot-type', type=str, choices=plot_funcs.keys())
    parser.add_argument('features', type=str)
    parser.add_argument('output', type=str)
    args = parser.parse_args(sys.argv[1:])

    plot_func = plot_funcs[args.plot_type]
    plot_func(args)
