import logging, sys
import pandas as pd

def load_data(f):
    d = pd.DataFrame.from_csv(f)
    return d

def output_data(d, f):
    d.to_csv(f)

def update_precsion(d, precision):
    format_str = r"{:.%df}" % precision
    update = lambda x : float(format_str.format(x))
    d['infogain'] = [update(x) for x in d['infogain']]
    d['mixture_overlap_prob'] = [update(x) for x in d['mixture_overlap_prob']]

def select_kbest(d, k, sort_by):
    if sort_by == 'mixture_overlap_prob':
        d = d.sort(columns=['mixture_overlap_prob', 'infogain'], 
                ascending=[True, False]).head(k)
    elif sort_by == 'infogain':
        d = d.sort(columns=['infogain', 'mixture_overlap_prob'], 
                ascending=[False, True]).head(k)
    return d

if __name__ == "__main__":
    import argparse
    logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)
    parser = argparse.ArgumentParser(description="Select top k features by\
            their information gains or mixture overlap probabilities.")
    parser.add_argument('--k', type=int, default=500, help="Default 500")
    parser.add_argument('--precision', type=int, default=3, 
            help="Number of decimal digits to consider when ranking.")
    parser.add_argument('--sort-by', type=str, default='infogain',
            choices=['infogain','mixture_overlap_prob'],
            help="Sort the features by either information gains or \
                    mixture overlap probabilities.")
    parser.add_argument('features', type=str, help="The feature file\
            from infogain output.")
    parser.add_argument('output', type=str, help="The output of the selected\
            top k features.")
    args = parser.parse_args(sys.argv[1:])

    logging.info("Loading feature file")
    d = load_data(args.features)

    logging.info("Update precision to %d decimal points"
            % args.precision)
    update_precsion(d, args.precision)

    logging.info("Selecting top %d features by %s" % (args.k, args.sort_by))
    d = select_kbest(d, args.k, args.sort_by)

    logging.info("Output selected features to %s" % args.output)
    output_data(d, args.output)
