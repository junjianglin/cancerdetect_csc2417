\documentclass[12pt]{article}
\usepackage{cite}
\usepackage{setspace}
\begin{document}
% \doublespacing

\title{Epigenetic Biomarker Discovery Approaches in Circulating DNA Tiling
Array Data}
\author{Junjiang Lin, Erkang Zhu}
\maketitle

% \linespread{2}

\section{Introduction}

\subsection{Problem overview}

A great deal of research has been invested in improving treatment for advanced
disease, because most people who develop cancer have advanced disease at the
time of diagnosis. For example, patients who have lung, colorectal or breast
cancer, in the United States, 72\%, 57\%, 34\%, respectively, have regional or
distant spread of their disease at the time of diagnosis \cite{zitt2007dna}. Early diagnosis
of cancer is so far the best way to reduce cancer-related mortality.
By detecting tumor at an early stage, the chances for available treatment
options to be successful increase dramatically \cite{park2012search}. Therefore, early diagnosis
in cancer treatment will be a very effective way to improve cancer prognosis.

% \vspace{5 mm}
% \noindent
Traditional cancer diagnosis is based on assessing the morphology of cancer
cells. However, in some cancers whose cells are not easily accessible,
diagnosis by cancer cells requires tumor biopsies and this would be a pain
for patient. Identification of cancer-specific epigenetic DNA alterations in
the cell-free circulating DNA (cirDNA) is a new opportunity for cancer
diagnosis and screening, especially for cancers whose cells are not easily accessible,
such as colorectal cancer \cite{park2012search}. In this study, we present several approaches
to investigate and select biomarkers at the level of DNA methylation in cirDNA
of individuals affected with colorectal cancer.


\subsection{Epigenetic alterations, DNA methylations}

Epigenetics alternation refers to heritable changes in gene expression that
are not based on DNA sequence. And DNA methylation is one of the common
mechanisms in epigenetic alteration.  In differentiated mammalian cells,
modified cytosines are detected predominantly in cytosine-guanine (CpG)
dinucleotides, the majority of which are clustered in CpG-rich areas typically
related to transcription start sites. These CpG-rich regions are referred to
as ``CpG islands'' and are mainly unmethylated, while remainder of the genome
is mainly methylated \cite{cortese2012epigenetic}.

% \vspace{5 mm}
% \noindent
Abnormal patterns of DNA methylation are commonly found in cancer study.
Cancer cells exhibit a global loss of DNA methylation as well as a gain of
methylation in some CpG islands. And these alterations provide tumor cells
with an advantage by increasing genetic instability and allowing them to grow
progressively \cite{park2012search}.


\subsection{Circulating DNA}

Cell-free circulating DNA refers to fragments of extracellular DNA that flow
freely in the circulation, and it originates from dead cells through
apoptosis and necrosis. Circulating DNA levels have been widely reported to
be elevated in many cancer cells, this is mainly because tumor cells usually
have higher rates of cell apoptosis and necrosis. Therefore, circulating DNA
has been a focus of biomarker research in cancer study since tumor cells were
shown to release their DNA into blood \cite{schwarzenbach2011cell}.  And detecting DNA methylation is
a promising area for discovering biomarkers in circulating DNA.

\section{Data}

\subsection{Samples info}
The sample set for this experiment contains 193 patients diagnosed with
colorectal cancer and 200 unaffected individuals with various ages ranging
from 37 to 90 years old. In both of the study groups, the number of male and
female individuals are equivalent to avoid sex bias. All plasma samples of
the colorectal cancer patients are provided by the Ontario Tumor Bank in
Toronto, Canada. And the samples in control group are provided by the Colon
Family Registry in National Institutes of Health, USA.

\subsection{Tiling array data}
Samples from the colorectal cancer study set were hybridized to Affymetrix
FeneChip Human Tiling 2.0R Array Set. The array was specifically designed to
interrogate genomic regions of chromosome 4, 15, 18, and 20 using around 6
 millions probes in total at a 35-bp resolutions, which means all the probes
 are 25-bp long in length with a 10-bp gap between each other.

% \vspace{5 mm}
% \noindent
Essentially, the goal of this experiment is to find the
differentially methylated DNA regions and use them to set up a set of
epigenetic biomarkers for the diagnosis of colorectal cancer.
But the methods we will discuss in next section could be extended to all
kinds of epigenetics study using tiling array data.

% \vspace{5 mm}
% \noindent
The theory behind this is that all methylated DNA regions should
contain one or more restriction enzyme (RE) sites recognized by one of the
three methylation-sensitive enzymes: HpaII (CCGG), HinP1I (GCGC),
HpyCH4IV (ACGT). The 4-mer in the parentheses denotes the RE site for the
corresponding enzyme. More specifically, a free circular DNA fragment
containing these RE sites can only be digested by the restriction enzymes
if the CpGs of these sites are unmethylated. Morevoer, only the undigested
DNA fragment will be amplified through PCR experiment. Therefore, a piece
of DNA fragment with methylated RE-site in the samples will hybridize to
the corresponding probes with higher rate and thus elicit higher probe
intensity than the same DNA fragment with unmethylated RE-site.


\section{Methodology}

We consider a tiling array as a data point and each probe as a feature.
We assume all data points fall into either of the two classes:
cancer and normal.
We can then formulate the problem of
finding the differential DNA regions as a feature selection problem:
search for the subset of features that is most relevant to the classification
outputs.
Saeys et al. \cite{saeys2007review}
conducted an extensive survey of existing techniques for
feature selection in bioinformatics.
Generally there are three categories:
\textit{filter}, \textit{wrapper}, and \textit{embedded}.
Filter techniques search for the relevant features using
only statistics computed from the data, and they have the advantage of being
fast and scalable to high-dimensional datasets.
Wrapper techniques make model hypothesis
search a part of feature selection: possible features subsets are fed into
the classifier model training,
and cross-validation is used to evaluate the merit of the trained classifiers.
The relevance of a feature subset is determined by the performance of the
corresponding classifier.
Wrapper techniques usually give better
classification result, however,
they are computationally more expensive due to the
exponential growth of the space of all feature subsets,
and have higher risk of over-fitting.
Embedded techniques makes feature selection part of the classifier
construction. They are much less expensive than wrapper techniques.

Since we are working with 6 millions features,
filter techniques are computationally cheaper since they scales very well to
high-dimensions.
% Researchers have used \textit{filter} techniques on microarray data
% \cite{golub1999molecular, chow2001identifying, dudoit2002comparison}.
On the other hand, wrapper techniques produce much better classifier
than filter techniques.
Xing et al.
\cite{xing2001feature} used both filter and wrapper
techniques: select high-relevance features using information gain
filter and pass them into Markov blanket filtering
\cite{ilprints208} to generate candidate feature subsets,
then feed the feature subsets into a classification algorithm for model
training, and use cross-validation to determine the best model.
Markov blanket filtering is a sequential
approximation algorithm that is much more efficient than combinatorial search
over all subsets.
Also, since the space of feature subsets is substantially reduced by the
filter step, the wrapper step gets speed up greatly.
Xing et al. compared this
hybrid approach to regularization methods, which does not remove features,
and showed that the hybrid approach yields better classifier.
In our work, we are going to replicate this approach on our dataset,
and report the performance.

The second approach is the Minimum-Redundancy-Maximum-Relevance (MRMR)
framework proposed by Ding and Peng \cite{ding2005minimum}.
MRMR is a filter technique. Its core idea is that in a representative feature
set, features are minimally similar to each other, and at the same time
maximally relevant to the target classes.
For discrete feature variables, Ding and Peng used mutual information
to represent the similarity between features as well as the the relevance
between features and target classes.
For continuous feature variables, they used F-test correlation for the
similarity and relevance measure.
To tackle the exponential search space of feature subsets,
the work proposed a heuristic approximation algorithm to solve the
MRMR optimization problem.
Later work by Peng et al. \cite{peng2005feature} enhanced this approach
by combining it with a wrapper technique that searches for a refined subset
of the features returned by the MRMR optimization.
This is similar to Xing et al.'s approach in that they both
used some filter techniques to prune the
space of feature subsets fed to wrapper algorithms.
In our work, we are going to use Peng et al.'s approach on our dataset, and
report the performance.

The third approach to detect the differential DNA regions
is Hidden Markov Model (HMM). HMM can be used in any markov process with
hidden states. We can formalize the problem into a HMM: 
\begin{itemize}
	\item Hidden states we are going to predict: normal state that
makes no difference between Case and Control samples, and cancer state
that elicits different probe intensities between Case and Control samples.
	\item Observed states we observe in the experiment: statistical p-value that represents estimated probability of rejecting the probe/feature is in normal state. 
	\item Transition probability: can be calculated by maximum likelihood approach.
	\item Emission probability: since p value is a probability, we can use parametric method to approximately model the emission probability as a Beta distribution.
	\item Initial probablity: can be calculated by clustering all p values into two classes. 
\end{itemize}
Since epigenetic alteration is a continuous effect, adjacent features 
tend to have the same hidden states. This is also why HMM may work. HMM would be able to tolerate noises
and predict most-likely hidden states for all features. After that, we just
need to group adjacent features with predicted cancer states because previous research show that the differential regions tend to be 500-600 bp long in DNA sequence and each feature only represents 25 bp sequence \cite{cortese2012epigenetic}.

In the end, we are going to build classifers based on all the approaches described previously, and compare their performances to see which approach is the best.

\bibliography{ref}{}
\bibliographystyle{plain}
\end{document}
