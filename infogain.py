'''
Implements information gain filter with GMM quantization of features.
'''
import logging
import pandas as pd
import numpy as np
from sklearn.mixture import GMM
from sklearn.base import TransformerMixin
from sklearn.base import BaseEstimator

from util import load_data

def quantize(X, y):
    '''
    Quantize the data X
    Return an array of mixture overlap probabilities of all features
    '''
    n_samples, n_features = X.shape
    gmms = [GMM(n_components=2, covariance_type='diag')
            for _ in range(n_features)]
    # Fit GMM for every feature separately
    for j, values in enumerate(X.T):
        gmms[j].fit(values)
    # Assign quantized values to every feature of every data point
    # based on posterior probability
    for j in range(n_features):
        values = X[:,j]
        q = gmms[j].predict(values)
        means = gmms[j].means_
        # We need to flip if lower expression level label becomes 1
        # instead of 0
        if means[0] > means[1]:
            q = 1 - q
        X[:,j] = q
    # Compute the mixture overlap probability
    e = np.sum((X.T == 1)*(y == 0) + (X.T == 0)*(y == 1), axis=1)
    e = e.astype(np.float) / n_samples
    return e

def _entropy(fv):
    p_on = np.sum(fv == 1).astype(np.float) / fv.size
    p_off = 1 - p_on
    return (- p_on * np.log2(p_on)) + (- p_off * np.log2(p_off))

def information_gain(X, y):
    '''
    Return
    array, shape = (n_features,) The information gains of all features
    '''
    (n_samples, n_features) = X.shape
    # The original entroy
    H_S = _entropy(y)
    gs = np.zeros((n_features,), np.float)
    for i, fv in enumerate(X.T):
        f_on = (fv == 1)
        f_off = (fv == 0)
        H_y_f_on = _entropy(y[f_on])
        H_y_f_off = _entropy(y[f_off])
        p_f_on = np.sum(f_on).astype(np.float) / n_samples
        p_f_off = 1 - p_f_on
        gs[i] = H_S - (p_f_on * H_y_f_on + p_f_off * H_y_f_off)
    return gs

def output_info_gain(feature_labels, info_gain, mixture_overlap, filename):
    d = pd.DataFrame(index=feature_labels) 
    d['infogain'] = info_gain
    d['mixture_overlap_prob'] = mixture_overlap
    d.to_csv(filename)

if __name__ == "__main__":
    import sys, argparse
    logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)
    parser = argparse.ArgumentParser(description="Information Gain Filter")
    parser.add_argument('--k', type=int, help="The best k features by their\
            information gain to be used in Markove Blanket Filtering.\
            All features will be selected if not specified.")
    parser.add_argument('train', type=str, help="The training dataset.")
    parser.add_argument('output', type=str, help="The selected features")
    args = parser.parse_args(sys.argv[1:])

    logging.info("Loading training data file %s" % args.train)
    X, y, sample_labels, feature_labels = load_data(args.train)

    logging.info("Quantizing training data")
    e = quantize(X, y)

    logging.info("Computing information gain of all features")
    info_gain = information_gain(X, y)

    if args.k is not None:
        logging.info("Selecting the best %d features by information gain"
                % args.k)
        best_k_ind = np.argpartition(info_gain, -args.k)[-args.k:]
        X = X[:,best_k_ind]
        info_gain = info_gain[best_k_ind]
        e = e[best_k_ind]
        feature_labels = feature_labels[best_k_ind]

    logging.info("Output selected features' information gain and mixture overlap probabilities to %s"
            % args.output)
    output_info_gain(feature_labels, info_gain, e, args.output)

