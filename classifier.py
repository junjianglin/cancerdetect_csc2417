'''
Training and test classifiers
'''
import logging
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.metrics import roc_auc_score

from util import load_selected_data, load_data

def load_markov_blanket_selected(f, k):
    df = pd.DataFrame.from_csv(f).tail(k)
    return df.index

def init_classifiers():
    return {'logistic_regression' : LogisticRegression(),
            'knn' : KNeighborsClassifier(n_neighbors=3),
            'svc' : SVC(probability=True)}

def init_results(classifiers):
    result = {}
    for name in classifiers:
        result[name] = {'train' : [], 'test' : []}
    return result

def compute_auc(classifier, X_test, y_test):
    i = list(classifier.classes_).index(1)
    y_score = classifier.predict_proba(X_test)[:,i]
    auc = roc_auc_score(y_test, y_score)
    return auc

def run(X_train, y_train, X_test, y_test, features):
    # Initialize classifiers
    classifiers = init_classifiers()
    results = init_results(classifiers)
    i = 0
    while i < len(features):
        n_features = len(features) - i
        logging.info("Using top %d features" % (len(features) - i))
        # Training
        for name, classifier in classifiers.items():
            logging.info("Training classifier %s" % name)
            classifier.fit(X_train[:,i:], y_train)
            score = compute_auc(classifier, X_train[:,i:], y_train)
            logging.info("Getting training score %f" % score)
            results[name]['train'].append((n_features, score))
        # Testing
        for name, classifier in classifiers.items():
            logging.info("Testing classifier %s" % name)
            score = compute_auc(classifier, X_test[:,i:], y_test)
            logging.info("Getting test score %f" % score)
            results[name]['test'].append((n_features, score))
        i += 1
    return results

if __name__ == "__main__":
    import argparse, sys
    logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument('--selected-features', type=str, help="The input file of \
            selected features from markovblanket.py, all features from data will \
            be used if this is not sepecified")
    parser.add_argument('--k', type=int, default=100, help="The top k features to\
            use from markovblanket output, default 100")
    parser.add_argument('--output', type=str, default="classifiers.png",
            help="The output file of classification result.")
    parser.add_argument('train', type=str, help="The traning data")
    parser.add_argument('test', type=str, help="The testing data")
    args = parser.parse_args(sys.argv[1:])

    if args.selected_features is not None:
        logging.info("Loading %d selected features from %s"
                % (args.k, args.selected_features))
        selected_features = \
                load_markov_blanket_selected(args.selected_features, args.k)
        logging.info("Loading training data %s and select %d features" %
                (args.train, len(selected_features)))
        X_train, y_train, sample_labels_train, feature_labels = \
                load_selected_data(args.train, selected_features)
        logging.info("Loading testing data %s and select %d features" %
                (args.test, len(selected_features)))
        X_test, y_test, sample_labels_test, _ = \
                load_selected_data(args.test, selected_features)
    else:
        logging.info("Loading complete training data %s" % args.train)
        X_train, y_train, sample_labels_train, feature_labels = \
                load_data(args.train)
        logging.info("Loading complete testing data %s" % args.test)
        X_test, y_test, sample_labels_test, _ = \
                load_data(args.test)

    results = run(X_train, y_train, X_test, y_test, feature_labels)

    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    fig, axis = plt.subplots(1, len(results), sharey=True, figsize=(5*len(results), 5))

    for i, (name, result) in enumerate(results.items()):
        n_features, train_scores = np.array(result['train']).T
        axis[i].plot(n_features, train_scores, label="training score")
        n_features, test_scores = np.array(result['test']).T
        axis[i].plot(n_features, test_scores, label="testing score")
        axis[i].set_xlabel("number of features")
        axis[i].set_ylabel("AUC score")
        axis[i].set_title(name)
        axis[i].legend(loc='lower right')
    fig.savefig(args.output)

    # Dump the result to a file
    import json
    json.dump(results, open("classifiers.json", 'w'))

